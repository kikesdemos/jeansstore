const stompClient = new StompJs.Client({
    brokerURL: 'ws://localhost:8080/customers-websocket'
});

stompClient.onConnect = (frame) => {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/average-reviews', (averageReviews) => {
        showAverageReviews(JSON.parse(averageReviews.body).stars);
    });
};

stompClient.onWebSocketError = (error) => {
    console.error('Error with websocket', error);
};

stompClient.onStompError = (frame) => {
    console.error('Broker reported error: ' + frame.headers['message']);
    console.error('Additional details: ' + frame.body);
};

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#average-reviews-table").show();
    }
    else {
        $("#average-reviews-table").hide();
    }
    $("#average-reviews").html("");
}

function connect() {
    stompClient.activate();
}

function disconnect() {
    stompClient.deactivate();
    setConnected(false);
    console.log("Disconnected");
}

function sendReview() {
    stompClient.publish({
        destination: "/app/add-review",
        body: JSON.stringify({'name': $("#name").val(), 'stars': $("#stars").val()})
    });
}

function showAverageReviews(stars) {
    $("#average-reviews").html("<tr><td>" + stars + "</td></tr>");
}

$(function () {
    $("form").on('submit', (e) => e.preventDefault());
    $( "#connect" ).click(() => connect());
    $( "#disconnect" ).click(() => disconnect());
    $( "#send" ).click(() => sendReview());
});