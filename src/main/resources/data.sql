INSERT INTO "sizes" ("value") VALUES ('26');
INSERT INTO "sizes" ("value") VALUES ('28');
INSERT INTO "sizes" ("value") VALUES ('29');
INSERT INTO "sizes" ("value") VALUES ('30');
INSERT INTO "sizes" ("value") VALUES ('31');
INSERT INTO "sizes" ("value") VALUES ('32');
INSERT INTO "sizes" ("value") VALUES ('33');
INSERT INTO "sizes" ("value") VALUES ('34');
INSERT INTO "sizes" ("value") VALUES ('35');
INSERT INTO "sizes" ("value") VALUES ('36');
INSERT INTO "sizes" ("value") VALUES ('38');
INSERT INTO "sizes" ("value") VALUES ('40');
INSERT INTO "sizes" ("value") VALUES ('42');
INSERT INTO "sizes" ("value") VALUES ('44');

INSERT INTO "fits" ("value") VALUES ('Straight');
INSERT INTO "fits" ("value") VALUES ('Original');
INSERT INTO "fits" ("value") VALUES ('Slim');
INSERT INTO "fits" ("value") VALUES ('Taper');

INSERT INTO "colors" ("value") VALUES ('Black');
INSERT INTO "colors" ("value") VALUES ('Blue');
INSERT INTO "colors" ("value") VALUES ('Brown');
INSERT INTO "colors" ("value") VALUES ('Dark Wash');
INSERT INTO "colors" ("value") VALUES ('Green');
INSERT INTO "colors" ("value") VALUES ('Grey');
INSERT INTO "colors" ("value") VALUES ('Light Wash');
INSERT INTO "colors" ("value") VALUES ('Medium Wash');
INSERT INTO "colors" ("value") VALUES ('Pink');
INSERT INTO "colors" ("value") VALUES ('Purple');
INSERT INTO "colors" ("value") VALUES ('White');

INSERT INTO "categories" ("name") VALUES ('ROOT');
INSERT INTO "categories" ("name", "parent_id") VALUES ('MEN', 1);
INSERT INTO "categories" ("name", "parent_id") VALUES ('WOMEN', 1);
INSERT INTO "categories" ("name", "parent_id") VALUES ('KIDS', 1);
INSERT INTO "categories" ("name", "parent_id") VALUES ('SALE', 1);

INSERT INTO "products" ("name", "description") VALUES ('501 ORIGINAL FIT MEN''S JEANS','The original blue jeans. 100% cotton');

INSERT INTO "categories_products" ("category_id", "product_id") VALUES (2, 1);
INSERT INTO "categories_products" ("category_id", "product_id") VALUES (5, 1);




