package com.jeansstore.jeansstore.repository;

import com.jeansstore.jeansstore.entity.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductImageRepository extends JpaRepository<ProductImage, Long> {
    public Optional<ProductImage> findByProductId(Long productId);
}
