package com.jeansstore.jeansstore.repository;

import com.jeansstore.jeansstore.entity.Fit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FitRepository extends JpaRepository<Fit, Long> {
}
