package com.jeansstore.jeansstore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name="fits")
public class Fit {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name="value")
    private String value;

    @OneToMany(mappedBy = "fit")
    @JsonIgnore
    private List<Sku> skus;

    public Fit() {
    }

    public Fit(Long id, String value, List<Sku> skus) {
        this.id = id;
        this.value = value;
        this.skus = skus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Sku> getSkus() {
        return skus;
    }

    public void setSkus(List<Sku> skus) {
        this.skus = skus;
    }
}
