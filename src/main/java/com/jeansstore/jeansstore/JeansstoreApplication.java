package com.jeansstore.jeansstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeansstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(JeansstoreApplication.class, args);
	}

}
