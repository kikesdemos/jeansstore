package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.Category;
import com.jeansstore.jeansstore.entity.Product;
import com.jeansstore.jeansstore.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductService productService;
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    public Optional<Category> getById(Long id) {
        return categoryRepository.findById(id);
    }

    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    public Optional<Category> addProduct(Long categoryId, Product product) {
        Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
        Optional<Product> optionalPersistedProduct = productService.getById(product.getId());

        if (optionalCategory.isPresent() && optionalPersistedProduct.isPresent()) {
            Category category = optionalCategory.get();
            Product persistedProduct = optionalPersistedProduct.get();
            persistedProduct.addParentCategory(category);
            category.addProduct(persistedProduct);
            return Optional.of(categoryRepository.save(category));
        }

        return optionalCategory;
    }

    public Optional<Category> updateById(Long id, Category category) {
        Optional<Category> storedCategory = categoryRepository.findById(id);

        if (storedCategory.isPresent()) {
            category.setId(id);
            Category savedCategory = categoryRepository.save(category);
            return Optional.of(savedCategory);
        } else {
            return Optional.empty();
        }
    }

    public Optional<Category> deleteById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);

        category.ifPresent(value -> categoryRepository.delete(value));

        return category;
    }
}
