package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.*;
import com.jeansstore.jeansstore.repository.SkuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class SkuService {
    public static final String COMMA_SEPARATOR = ",";
    public static final int ID_FIELD = 0;
    public static final int PRICE_FIELD = 1;
    public static final int ACTIVE_FIELD = 2;
    public static final int PRODUCT_ID_FIELD = 3;
    public static final int COLOR_ID_FIELD = 4;
    public static final int SIZE_ID_FIELD = 5;
    public static final int FIT_ID_SIZE = 6;
    public static final String NEW_LINE_SEPARATOR = "\n";
    @Autowired
    private SkuRepository skuRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ColorService colorService;

    @Autowired
    private SizeService sizeService;

    @Autowired
    private FitService fitService;

    public List<Sku> getAll() {
        return skuRepository.findAll();
    }

    public Optional<Sku> getById(Long id) {
        return skuRepository.findById(id);
    }

    public Sku create(Sku sku) {
        return skuRepository.save(sku);
    }

    public Optional<Sku> updateById(Long id, Sku sku) {
        Optional<Sku> storedProduct = skuRepository.findById(id);

        if (storedProduct.isPresent()) {
            sku.setId(id);
            Sku savedSku = skuRepository.save(sku);
            return Optional.of(savedSku);
        } else {
            return Optional.empty();
        }
    }

    public Optional<Sku> deleteById(Long id) {
        Optional<Sku> sku = skuRepository.findById(id);

        sku.ifPresent(value -> skuRepository.delete(value));

        return sku;
    }

    public void upload(final InputStream inputStream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;

        while ((line = br.readLine()) != null) {
            String[] values = line.split(COMMA_SEPARATOR);
            Long skuId = Long.parseLong(values[ID_FIELD]);
            BigDecimal price = new BigDecimal(values[PRICE_FIELD]);
            Boolean active = Boolean.parseBoolean(values[ACTIVE_FIELD]);
            Long productId = Long.parseLong(values[PRODUCT_ID_FIELD]);
            Optional<Product> product = productService.getById(productId);
            Long colorId = Long.parseLong(values[COLOR_ID_FIELD]);
            Optional<Color> color = colorService.getById(colorId);
            Long sizeId = Long.parseLong(values[SIZE_ID_FIELD]);
            Optional<Size> size = sizeService.getById(sizeId);
            Long fitId = Long.parseLong(values[FIT_ID_SIZE]);
            Optional<Fit> fit = fitService.getById(fitId);
            Sku sku = new Sku(skuId, price, active, product.orElseThrow(), color.orElseThrow(), size.orElseThrow(), fit.orElseThrow());

            skuRepository.save(sku);
        }
    }

    public byte[] download() {
        List<Sku> skus = skuRepository.findAll();
        StringBuilder csvContent = new StringBuilder();

        for (final Sku sku : skus) {
            csvContent.append(sku.getId()).append(COMMA_SEPARATOR)
                    .append(sku.getPrice()).append(COMMA_SEPARATOR)
                    .append(sku.getActive()).append(COMMA_SEPARATOR)
                    .append(sku.getProduct().getId()).append(COMMA_SEPARATOR)
                    .append(sku.getColor().getId()).append(COMMA_SEPARATOR)
                    .append(sku.getSize().getId()).append(COMMA_SEPARATOR)
                    .append(sku.getFit().getId()).append(NEW_LINE_SEPARATOR);
        }

        return csvContent.toString().getBytes();
    }
}
