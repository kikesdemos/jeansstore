package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.Category;
import com.jeansstore.jeansstore.entity.Product;
import com.jeansstore.jeansstore.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public Optional<Product> getById(Long id) {
        return productRepository.findById(id);
    }

    public Product create(Product product) {
        return productRepository.save(product);
    }

    public Optional<Product> updateById(Long id, Product product) {
        Optional<Product> storedProduct = productRepository.findById(id);

        if (storedProduct.isPresent()) {
            product.setId(id);
            Product savedProduct = productRepository.save(product);
            return Optional.of(savedProduct);
        } else {
            return Optional.empty();
        }
    }

    public Optional<Product> deleteById(Long id) {
        Optional<Product> product = productRepository.findById(id);

        product.ifPresent(value -> productRepository.delete(value));

        return product;
    }
}
