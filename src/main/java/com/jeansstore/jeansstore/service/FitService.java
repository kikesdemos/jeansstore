package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.Fit;
import com.jeansstore.jeansstore.repository.FitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FitService {
    @Autowired
    FitRepository fitRepository;

    public List<Fit> getAll() {
        return fitRepository.findAll();
    }

    public Optional<Fit> getById(Long id) {
        return fitRepository.findById(id);
    }
}
