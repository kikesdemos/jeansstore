package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.Color;
import com.jeansstore.jeansstore.repository.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ColorService {
    @Autowired
    ColorRepository colorRepository;

    public List<Color> getAll() {
        return colorRepository.findAll();
    }

    public Optional<Color> getById(Long id) {
        return colorRepository.findById(id);
    }
}
