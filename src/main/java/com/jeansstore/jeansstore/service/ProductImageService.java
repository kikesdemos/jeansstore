package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.ProductImage;
import com.jeansstore.jeansstore.repository.ProductImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductImageService {
    @Autowired
    private ProductImageRepository ProductImageRepository;

    public List<ProductImage> getAll() {
        return ProductImageRepository.findAll();
    }

    public Optional<ProductImage> getById(Long id) {
        return ProductImageRepository.findById(id);
    }

    public Optional<ProductImage> getByProductId(Long productId) {
        return ProductImageRepository.findByProductId(productId);
    }

    public ProductImage create(ProductImage productImage) {
        return ProductImageRepository.save(productImage);
    }

    public Optional<ProductImage> updateById(Long id, ProductImage productImage) {
        Optional<ProductImage> storedProductImage = ProductImageRepository.findById(id);

        if (storedProductImage.isPresent()) {
            productImage.setId(id);
            ProductImage savedProductImage = ProductImageRepository.save(productImage);
            return Optional.of(savedProductImage);
        } else {
            return Optional.empty();
        }
    }

    public Optional<ProductImage> deleteById(Long id) {
        Optional<ProductImage> productImage = ProductImageRepository.findById(id);

        productImage.ifPresent(value -> ProductImageRepository.delete(value));

        return productImage;
    }
}
