package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.entity.Size;
import com.jeansstore.jeansstore.repository.SizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SizeService {
    @Autowired
    SizeRepository sizeRepository;

    public List<Size> getAll() {
        return sizeRepository.findAll();
    }

    public Optional<Size> getById(Long id) {
        return sizeRepository.findById(id);
    }
}
