package com.jeansstore.jeansstore.service;

import com.jeansstore.jeansstore.model.ReviewsAverage;
import com.jeansstore.jeansstore.model.CustomerReview;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ReviewService {
    Map<Integer, Long> reviews = new HashMap<>();

    public ReviewService() {
        reviews.put(1, 0L);
        reviews.put(2, 0L);
        reviews.put(3, 0L);
        reviews.put(4, 0L);
        reviews.put(5, 0L);
    }

    public void addReview(CustomerReview customerReview) {
        long currentReviewForGivenRate = reviews.get(customerReview.getStars());
        reviews.put(customerReview.getStars(), ++currentReviewForGivenRate);
    }

    public ReviewsAverage average() {
        ReviewsAverage reviewsAverage = new ReviewsAverage();
        long accumulated = 0L;
        long count = 0L;

        for(Map.Entry<Integer, Long> entry : reviews.entrySet()) {
            count += entry.getValue();
            accumulated += entry.getKey() * entry.getValue();
        }

        long average = accumulated / count;
        reviewsAverage.setStars((int) average);

        return reviewsAverage;
    }
}
