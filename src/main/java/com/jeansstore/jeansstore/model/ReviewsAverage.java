package com.jeansstore.jeansstore.model;

public class ReviewsAverage {
    private int stars;

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
