package com.jeansstore.jeansstore.model;

import com.jeansstore.jeansstore.entity.Product;
public class CustomerReview {
    private String name;
    private int stars;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
