package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.entity.Size;
import com.jeansstore.jeansstore.service.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/catalog")
public class SizeController {
    @Autowired
    private SizeService sizeService;

    @GetMapping("/sizes")
    public ResponseEntity<List<Size>> getAll() {
        List<Size> sizes = sizeService.getAll();
        return new ResponseEntity<List<Size>>(sizes, HttpStatus.OK);
    }

    @GetMapping("/sizes/{id}")
    public ResponseEntity<Size> getById(@PathVariable Long id) {
        Optional<Size> size = sizeService.getById(id);

        return size.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
