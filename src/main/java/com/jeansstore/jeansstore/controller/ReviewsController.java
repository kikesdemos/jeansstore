package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.model.ReviewsAverage;
import com.jeansstore.jeansstore.model.CustomerReview;
import com.jeansstore.jeansstore.service.ReviewService;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ReviewsController {
    Logger logger = LoggerFactory.getLogger(ReviewsController.class);
    @Autowired
    ReviewService reviewService;

    @MessageMapping("/add-review")
    @SendTo("/topic/average-reviews")
    public ReviewsAverage average(@NotNull CustomerReview customerReview) throws Exception {
        logger.info("Customer Review: " + customerReview.getName() + " " + customerReview.getStars());
        reviewService.addReview(customerReview);
        return reviewService.average();
    }
}
