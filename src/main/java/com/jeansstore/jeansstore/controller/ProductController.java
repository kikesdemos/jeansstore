package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.entity.Product;
import com.jeansstore.jeansstore.entity.ProductImage;
import com.jeansstore.jeansstore.service.ProductImageService;
import com.jeansstore.jeansstore.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/catalog")
public class ProductController {

    public static final String JPEG_CONTENT_TYPE = "image/jpeg";
    Logger logger = LoggerFactory.getLogger(ProductController.class);
    @Autowired
    private ProductService productService;

    @Autowired
    private ProductImageService productImageService;

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAll() {
        List<Product> products = productService.getAll();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getById(@PathVariable Long id) {
        Optional<Product> product = productService.getById(id);

        return product.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/products")
    public ResponseEntity<Product> create(@RequestBody Product product) {
        Product savedProduct = productService.create(product);

        return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateById(@PathVariable Long id, @RequestBody Product product) {
        Optional<Product> savedProduct = productService.updateById(id, product);

        return savedProduct.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteById(@PathVariable Long id) {
        Optional<Product> deletedProduct = productService.deleteById(id);

        return deletedProduct.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PutMapping("/products/{productId}/product-images")
    public ResponseEntity<Object> uploadImage(@PathVariable Long productId, @RequestParam("image") MultipartFile imageFile) throws IOException {
        byte[] data = imageFile.getBytes();
        logger.info("Loaded file {} with {} bytes {} type", imageFile.getOriginalFilename(), imageFile.getSize(), imageFile.getContentType());

        if (JPEG_CONTENT_TYPE.equals(imageFile.getContentType())) {
            Optional<Product> product = productService.getById(productId);

            if (product.isPresent()) {
                Optional<ProductImage> optionalImage = productImageService.getByProductId(productId);
                ProductImage image = null;

                if (optionalImage.isPresent()) {
                    image = optionalImage.get();
                    image.setData(data);
                    productImageService.updateById(image.getId(), image);
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    image = new ProductImage();
                    image.setId(0L);
                    image.setProduct(product.get());
                    image.setData(data);
                    productImageService.create(image);
                    return new ResponseEntity<>(HttpStatus.CREATED);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
    }

    @GetMapping(
            value = "/products/{productId}/product-images",
            produces = JPEG_CONTENT_TYPE
    )
    public ResponseEntity<byte[]> downloadImage(@PathVariable Long productId) {
        Optional<ProductImage> image = productImageService.getByProductId(productId);
        return image.map(productImage -> new ResponseEntity<>(productImage.getData(), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
