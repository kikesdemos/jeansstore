package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.entity.Sku;
import com.jeansstore.jeansstore.service.SkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/catalog")
public class SkuController {
    @Autowired
    private SkuService skuService;

     @GetMapping("/skus")
     public ResponseEntity<List<Sku>> getAll() {
         List<Sku> skus = skuService.getAll();
         return new ResponseEntity<>(skus, HttpStatus.OK);
     }

    @GetMapping("/skus/{id}")
    public ResponseEntity<Sku> getById(@PathVariable Long id) {
        Optional<Sku> sku = skuService.getById(id);

        return sku.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/skus")
    public ResponseEntity<Sku> create(@RequestBody Sku Sku) {
        Sku savedSku = skuService.create(Sku);

        return new ResponseEntity<>(savedSku, HttpStatus.CREATED);
    }

    @PostMapping("/skus/upload")
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) throws IOException {
        List<Sku> skus = new ArrayList<>();

        String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        InputStream inputStream = file.getInputStream();
        skuService.upload(inputStream);

        return new ResponseEntity<>("Loaded " + filename, HttpStatus.OK);
    }

    @GetMapping("/skus/download")
    public ResponseEntity<byte[]> download() {
        byte[] content = skuService.download();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "skus.csv");

        return new ResponseEntity<>(content, headers, HttpStatus.OK);
    }

    @PutMapping("/skus/{id}")
    public ResponseEntity<Sku> updateById(@PathVariable Long id, @RequestBody Sku sku) {
        Optional<Sku> savedSku = skuService.updateById(id, sku);

        return savedSku.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @DeleteMapping("/skus/{id}")
    public ResponseEntity<Sku> updateById(@PathVariable Long id) {
        Optional<Sku> deletedSku = skuService.deleteById(id);

        return deletedSku.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
