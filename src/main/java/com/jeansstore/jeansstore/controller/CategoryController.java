package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.entity.Category;
import com.jeansstore.jeansstore.entity.Product;
import com.jeansstore.jeansstore.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/catalog")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

     @GetMapping("/categories")
     public ResponseEntity<List<Category>> getAll() {
         List<Category> categories = categoryService.getAll();
         return new ResponseEntity<>(categories, HttpStatus.OK);
     }

    @GetMapping("/categories/{id}")
    public ResponseEntity<Category> getById(@PathVariable Long id) {
        Optional<Category> category = categoryService.getById(id);

        return category.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/categories")
    public ResponseEntity<Category> create(@RequestBody Category category) {
        Category savedCategory = categoryService.create(category);

        return new ResponseEntity<>(savedCategory, HttpStatus.CREATED);
    }

    @PostMapping("/categories/{id}/products")
    public ResponseEntity<Category> addProduct(@PathVariable Long id, @RequestBody Product product) {
        Optional<Category> savedCategory = categoryService.addProduct(id, product);

        return savedCategory.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<Category> updateById(@PathVariable Long id, @RequestBody Category category) {
        Optional<Category> savedCategory = categoryService.updateById(id, category);

        return savedCategory.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<Category> updateById(@PathVariable Long id) {
        Optional<Category> deletedCategory = categoryService.deleteById(id);

        return deletedCategory.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
