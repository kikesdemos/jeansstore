package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.entity.Fit;
import com.jeansstore.jeansstore.service.FitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/catalog")
public class FitController {
    @Autowired
    private FitService fitService;

    @GetMapping("/fits")
    public ResponseEntity<List<Fit>> getAll() {
        List<Fit> fits = fitService.getAll();
        return new ResponseEntity<List<Fit>>(fits, HttpStatus.OK);
    }

    @GetMapping("/fits/{id}")
    public ResponseEntity<Fit> getById(@PathVariable Long id) {
        Optional<Fit> fit = fitService.getById(id);

        return fit.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
