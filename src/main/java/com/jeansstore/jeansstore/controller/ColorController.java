package com.jeansstore.jeansstore.controller;

import com.jeansstore.jeansstore.entity.Color;
import com.jeansstore.jeansstore.service.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/catalog")
public class ColorController {
    @Autowired
    private ColorService colorService;

    @GetMapping("/colors")
    public ResponseEntity<List<Color>> getAll() {
        List<Color> colors = colorService.getAll();
        return new ResponseEntity<List<Color>>(colors, HttpStatus.OK);
    }

    @GetMapping("/colors/{id}")
    public ResponseEntity<Color> getById(@PathVariable Long id) {
        Optional<Color> color = colorService.getById(id);

        return color.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
