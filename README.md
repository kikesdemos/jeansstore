Step 0. Initialize Empty project with:

Spring Boot 3.2.2

Java 17

Maven

Dependencies: 
- Spring Web
- Spring Data JPA
- H2 Database

Step 1. Create Entities, Repositories, Services and Controllers for the following value lists:
- Size
- Fit
- Color

Add only Get All and Get By Id endpoints

Generate the Id using AUTO Approach that is the default value for the GeneratedValue annotation

https://www.baeldung.com/jpa-strategies-when-set-primary-key

Add H2

https://www.baeldung.com/spring-boot-h2-database

Initialize Database

https://www.baeldung.com/spring-boot-h2-database

Step 2. Add swagger and postman collection

https://www.baeldung.com/spring-rest-openapi-documentation

swagger url will be
http://localhost:8080/swagger-ui-custom.html

Step 3. Add product entity, repository, service and controller

Step 4. Add sku entity, repository, service and controller

Add 1 to many relationships

Enabled h2 console http://localhost:8080/h2-console

Step 5. Changed the navigation from product to skus

Step 6. Add External Application Properties
https://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/html/boot-features-external-config.html
Step 7. Add import endpoint

https://medium.com/@patelsajal2/how-to-create-a-spring-boot-rest-api-for-multipart-file-uploads-a-comprehensive-guide-b4d95ce3022b

https://www.baeldung.com/reading-file-in-java

https://www.baeldung.com/java-csv-file-array

Step 8. Add export endpoint

https://naveenrk22.medium.com/how-to-create-a-csv-file-from-a-list-using-java-and-spring-boot-1c97d72dd978

Step 9. Add Category Entity, Repository, Service, Controller

Step 10. Add Self relation for Category Entity

Step 11. Add Many to Many relationship

https://www.baeldung.com/jpa-many-to-many

https://www.bezkoder.com/jpa-many-to-many/

Step 12. Add a web socket and a single html/js client

html client => http://localhost:8080/reviews.html

https://spring.io/guides/gs/messaging-stomp-websocket
https://www.baeldung.com/websockets-spring
https://www.baeldung.com/spring-boot-logging
https://docs.spring.io/spring-framework/reference/web/websocket/stomp/overview.html

Step 12. Add Product Image upload and download

https://www.freeimages.com/photo/jeans-1421757

Photo by <a href="/photographer/vierdrie-46406">vierdrie</a> on <a href="/">Freeimages.com</a>

https://www.baeldung.com/jpa-one-to-one

https://medium.com/shoutloudz/spring-boot-upload-and-download-images-using-jpa-b1c9ef174dc0

https://www.baeldung.com/spring-boot-thymeleaf-image-upload

https://www.bezkoder.com/spring-boot-upload-file-database/

https://www.baeldung.com/spring-mvc-set-json-content-type

https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types

http://localhost:8080/catalog/products/1/product-images


